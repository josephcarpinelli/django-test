#!/usr/bin/env python3

"""URL mapping for Django project."""

from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
]